Este pequeño proyecto consiste en enviar correo electrónicos con plantillas que estan en la web.
Por ejemplo para el envío de correo eléctronico enviamos la siguiente plantilla:
https://potencie.com/files/text/5ef539633b0af90b31da0baf.html

A continuación se muestra la estructura json a enviar:

{
	"contentmail": {
		"Nickname": "vflores",
		"Clave": "Pa$$W00rd"
	},
	"datasend": [
		{
			"mailpage": "https://potencie.com/files/text/5ef539633b0af90b31da0baf.html",
			"t_email": "",
			"t_cco": [],
			"t_cc": []
		}
	]
}

//Framework gingonic
go get -u github.com/gin-gonic/gin

//CORS en gingonic
go get -u github.com/gin-contrib/cors

//ORM para golang gingonic
go get -u github.com/jinzhu/gorm

//validacion de datos entrante ,en golang
go get -u gopkg.in/validator.v2

//instalacion para .env en golang
go get github.com/joho/godotenv

//crear el env con la estructura que esta de ejemplo con el nombre de env-example

//insertar en el codigo el password de aplicación de gmail en el archivo mail.go dentro de "helper" para poder enviar correos desde ese correo personal o corporativo

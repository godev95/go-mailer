package helper

import (
	"fmt"
	// "log"
	"bytes"
	"crypto/tls"
	"errors"
	"html/template"
	"io/ioutil"
	"net/http"
	"net/mail"
	"net/smtp"
	// "time"
	// "strings"
)

type DataEmailSignup struct {
	Nickname string
	Clave    string
}

func ActiveSendMail(email string, email_cco []string, email_cc []string, reqDataPage interface{}, mailpage string) (string, error, int) {

	sender := "victorfq18@gmail.com"       //correo electrónico
	passverification := "" //clave de aplicacion

	from := mail.Address{"POTENCIE", sender}
	to := mail.Address{"", email}
	subject := "Bienvenido"

	headers := make(map[string]string)
	headers["From"] = from.String()
	headers["to"] = to.String()
	headers["Subject"] = subject
	headers["Content-Type"] = `text/html; charset="UTF-8"`

	message := ""

	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}

	/////////////// codigo para consumir un archivo html mediante una url /////////////
	resp, err := http.Get(mailpage)

	if resp.StatusCode == 404 || resp.StatusCode == 400 || resp.StatusCode == 500 {

		defer resp.Body.Close()
		return "The template was not found", errors.New("status code different to 200"), 500
	}

	if err != nil {
		fmt.Println("------------------")
		fmt.Println(err)
		fmt.Println("------------------")
		return "The template was not found", err, 500
	}

	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	t, err := template.New("mailpage").Parse(string(body))

	if err != nil {

		return "The template was not parse", err, 500

	}

	data := reqDataPage

	buf := new(bytes.Buffer)

	//en este script al template lo almacena en un buf y tambien le ingresa la data
	err = t.Execute(buf, data)
	// checkErr(err)
	if err != nil {

		return "An error of buffer", err, 500

	}

	message += buf.String()

	servername := "smtp.gmail.com:465"
	host := "smtp.gmail.com"
	auth := smtp.PlainAuth("", sender, passverification, host)

	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}

	conn, err := tls.Dial("tcp", servername, tlsConfig)
	// checkErr(err)
	if err != nil {

		return "An error of tcp", err, 500

	}

	client, err := smtp.NewClient(conn, host)
	// checkErr(err)
	if err != nil {

		return "An error of smtp", err, 500

	}

	err = client.Auth(auth)
	// checkErr(err)
	if err != nil {

		return "An error auth client", err, 500

	}

	err = client.Mail(from.Address)
	// checkErr(err)
	if err != nil {

		return "An error of email address to the client", err, 500

	}

	err = client.Rcpt(to.Address)
	// checkErr(err)
	if err != nil {
		return "An error of email address to the client rcpt", err, 500
	}

	if len(email_cco) != 0 {

		for j := 0; j <= len(email_cco)-1; j++ {

			if errcc := client.Rcpt(email_cco[j]); errcc != nil {
				return "An error of email address to the client rctp cco", err, 500
			}

		}

	}

	if len(email_cc) != 0 {

		for z := 0; z <= len(email_cc)-1; z++ {

			if errcc := client.Rcpt(email_cc[z]); errcc != nil {
				return "An error of email address to the client rctp cco", err, 500
			}
		}
	}

	w, err := client.Data()
	// checkErr(err)
	if err != nil {

		return "An error sending data to the client", err, 500

	}

	_, err = w.Write([]byte(message))
	// checkErr(err)
	if err != nil {

		return "A typing error in bytes", err, 500

	}

	err = w.Close()
	// checkErr(err)
	if err != nil {

		return "An error closing the writing process", err, 500

	}

	client.Quit()

	return "Email sent successfully", nil, 200

}

func ActiveSendBodyEmail(email string, email_cco []string, email_cc []string, reqDataPage interface{}, bodymail string, fromText string, subjectText string) (string, error, int) {

	sender := "victorfq18@gmail.com"       //correo electrónico
	passverification := "" //clave de aplicacion

	from := mail.Address{fromText, sender}
	to := mail.Address{"", email}
	subject := subjectText

	headers := make(map[string]string)
	headers["From"] = from.String()
	headers["to"] = to.String()
	headers["Subject"] = subject
	headers["Content-Type"] = `text/html; charset="UTF-8"`

	message := ""

	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}

	message += bodymail

	servername := "smtp.gmail.com:465"
	host := "smtp.gmail.com"
	auth := smtp.PlainAuth("", sender, passverification, host)

	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}

	conn, err := tls.Dial("tcp", servername, tlsConfig)
	// checkErr(err)
	if err != nil {

		return "An error of tcp", err, 500

	}

	client, err := smtp.NewClient(conn, host)
	// checkErr(err)
	if err != nil {

		return "An error of smtp", err, 500

	}

	err = client.Auth(auth)
	// checkErr(err)
	if err != nil {

		return "An error auth client", err, 500

	}

	err = client.Mail(from.Address)
	// checkErr(err)
	if err != nil {

		return "An error of email address to the client", err, 500

	}

	err = client.Rcpt(to.Address)
	// checkErr(err)
	if err != nil {
		return "An error of email address to the client rcpt", err, 500
	}

	if len(email_cco) != 0 {

		for j := 0; j <= len(email_cco)-1; j++ {

			if errcc := client.Rcpt(email_cco[j]); errcc != nil {
				return "An error of email address to the client rctp cco", err, 500
			}

		}

	}

	if len(email_cc) != 0 {

		for z := 0; z <= len(email_cc)-1; z++ {

			if errcc := client.Rcpt(email_cc[z]); errcc != nil {
				return "An error of email address to the client rctp cco", err, 500
			}
		}
	}

	w, err := client.Data()
	// checkErr(err)
	if err != nil {

		return "An error sending data to the client", err, 500

	}

	_, err = w.Write([]byte(message))
	// checkErr(err)
	if err != nil {

		return "A typing error in bytes", err, 500

	}

	err = w.Close()
	// checkErr(err)
	if err != nil {

		return "An error closing the writing process", err, 500

	}

	client.Quit()

	return "Email sent successfully", nil, 200

}

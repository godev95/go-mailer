module github.com/controller

go 1.16

require (
	github.com/gin-gonic/gin v1.6.3
	gopkg.in/validator.v2 v2.0.0-20210331031555-b37d688a7fb0
)

package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/model"

	// "github.com/asaskevich/govalidator"
	"encoding/json"

	"github.com/helper"
	"gopkg.in/validator.v2"

	// "net/http"
	"fmt"
	"log"
	"os"
	"time"
)

func timeNow() string {

	//establecer el tiempo actual
	t := time.Now()
	//establecer la fecha actual bajo un formato de 2006-01-02
	datenow := t.Format("2006-01-02")
	//retornar valores
	return datenow

}

func saveLog(name_err string, body string, string_err string, status int, c *gin.Context) {

	datenow := timeNow()

	file, err := os.OpenFile("./logs/"+datenow+".log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644) // For read access.
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	logger := log.New(file, "", 0)

	//establecer el nivel de log, la lista de niveles son: emerg, alert, crit, error, warn, notice, debug
	// loglevel := errorlevel

	datequery := time.Time.Format(time.Now(), time.RFC1123)

	// logger.Println("["+datequery+"] - - "+ipcliente+" "+method+" "+
	// 		path+" "+useragent+" "+errormessage+" ", statuscode)
	logger.Println("["+datequery+"] - - "+c.Request.Method+" "+
		c.Request.Host+" "+c.Request.UserAgent()+" "+" ERROR "+name_err+" "+string_err+" "+body, status)

}

func convertCCO(cco string) []string {

	//convertir en byte el campo cco que indica a quien se le va a tiener que copiar
	byte_cco := []byte(cco)

	//el json cco tiene un campo email, lo cual esta plasmado en este modelo
	var copia_contact model.EmailContact

	//quitando el formato json
	json.Unmarshal(byte_cco, &copia_contact)

	var emails []string

	fmt.Println(len(copia_contact))

	for i := 0; i <= len(copia_contact)-1; i++ {

		emails = append(emails, copia_contact[i].Email)

	}

	//convertir el dato del campo email a una cadena string

	// email_cco := string(copia_contact.Email)

	return emails

}

func Simplemail(c *gin.Context) {

	var smd model.SimpleMailingData

	c.ShouldBind(&smd)

	json_input, _ := json.Marshal(smd)
	body := string(json_input)

	if err := validator.Validate(&smd); err != nil {

		serr := err.Error()
		saveLog("VALIDATOR", body, serr, 400, c)

		c.JSON(400, gin.H{
			"data":    serr,
			"message": "Data incorrect",
		})

	} else {

		reqDataPage := smd.Contentmail

		var msg string
		var status int

		var email_cco []string
		var email_cc []string

		for i := 0; i <= len(smd.Datasend)-1; i++ {

			mailpage := smd.Datasend[i].Mailpage
			email := smd.Datasend[i].TEmail
			cco := smd.Datasend[i].TCco
			cc := smd.Datasend[i].TCc

			for i := 0; i <= len(cco)-1; i++ {
				email_cco = append(email_cco, cco[i].TEmail)
			}

			for j := 0; j <= len(cc)-1; j++ {
				email_cc = append(email_cc, cc[j].TEmail)
			}

			//se jala la plantilla y su data que esta precargada en la base de datos

			msg, err, status = helper.ActiveSendMail(email, email_cco, email_cc, reqDataPage, mailpage)

			if status != 200 {

				serr := err.Error()
				saveLog("SIMPLESENDMAIL ", body, serr, status, c)
				c.JSON(status, gin.H{
					"code":    "success",
					"data":    nil,
					"message": msg,
				})

				return

			}

		}

		c.JSON(status, gin.H{
			"code":    "success",
			"data":    nil,
			"message": msg,
		})

	}

}

func EmailBody(c *gin.Context) {

	var smd model.EmailData

	c.ShouldBind(&smd)

	json_input, _ := json.Marshal(smd)
	body := string(json_input)

	if err := validator.Validate(&smd); err != nil {

		serr := err.Error()
		saveLog("VALIDATOR", body, serr, 400, c)

		c.JSON(400, gin.H{
			"data":    serr,
			"message": "Data incorrect",
		})

	} else {

		reqDataPage := smd.Contentmail

		var msg string
		var status int

		var email_cco []string
		var email_cc []string

		for i := 0; i <= len(smd.Datasend)-1; i++ {

			bodymail := smd.Datasend[i].Body
			from := smd.Datasend[i].From
			subject := smd.Datasend[i].Subject
			email := smd.Datasend[i].TEmail
			cco := smd.Datasend[i].TCco
			cc := smd.Datasend[i].TCc

			for i := 0; i <= len(cco)-1; i++ {
				email_cco = append(email_cco, cco[i].TEmail)
			}

			for j := 0; j <= len(cc)-1; j++ {
				email_cc = append(email_cc, cc[j].TEmail)
			}

			//se jala la plantilla y su data que esta precargada en la base de datos

			msg, err, status = helper.ActiveSendBodyEmail(email, email_cco, email_cc, reqDataPage, bodymail, from, subject)

			if status != 200 {

				serr := err.Error()
				saveLog("SIMPLESENDMAIL ", body, serr, status, c)
				c.JSON(status, gin.H{
					"code":    "success",
					"data":    nil,
					"message": msg,
				})

				return

			}

		}

		c.JSON(status, gin.H{
			"code":    "success",
			"data":    nil,
			"message": msg,
		})

	}

}

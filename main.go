package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/route"
)

func main() {

	router := gin.Default()

	route.Routing(router)

	//llamar al archivo de variables de entorno
	err := godotenv.Load()

	if err != nil {
		//mostrar el error del .env
		log.Fatal("Error loading .env file")
	}

	portserver := os.Getenv("SERVER_PORT")

	// gin.SetMode(gin.ReleaseMode)
	// router.RunTLS("0.0.0.0:5002", "./ssl/STAR_potencie_com.crt", "./ssl/STAR_potencie_com.pem")

	router.Run("0.0.0.0:" + portserver)

}

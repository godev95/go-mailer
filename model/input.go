package model

type EmailContact []struct {
	Email string `json:"email"`
}

type InputEmailValidate struct {
	TEmail string `json:"t_email"`
}

type MailingData struct {
	Contentmail map[string]string `json:"contentmail"`
	Datasend    []struct {
		OwnerNickname        string `validate:"min=1" json:"owner_nickname"`
		CreatorNickname      string `validate:"min=1" json:"creator_nickname"`
		Mailpage             string `validate:"min=1"`
		TEmail               string `json:"t_email"`
		InformationContactID int    `json:"information_contact_id"`
		ActivityID           int    `json:"activity_id"`
		ContactID            int    `json:"contact_id"`
		TCco                 []Copy `json:"t_cco"`
		TCc                  []Copy `json:"t_cc"`
	} `json:"datasend"`
}

type SimpleMailingData struct {
	Contentmail map[string]string `json:"contentmail"`
	Datasend    []struct {
		Mailpage string `validate:"min=1"`
		TEmail   string `json:"t_email"`
		TCco     []Copy `json:"t_cco"`
		TCc      []Copy `json:"t_cc"`
	} `json:"datasend"`
}

type Copy struct {
	ContactID            int    `json:"contact_id"`
	TEmail               string `json:"t_email"`
	InformationContactID int    `json:"information_contact_id"`
}

// INTERFACES
type error interface {
	Error() string
}

//Email simple body
type EmailData struct {
	Contentmail map[string]string `json:"contentmail"`
	Datasend    []struct {
		Body    string `validate:"min=1"`
		From    string `json:"from"`
		Subject string `json:"subject"`
		TEmail  string `json:"t_email"`
		TCco    []Copy `json:"t_cco"`
		TCc     []Copy `json:"t_cc"`
	} `json:"datasend"`
}

package model

import (
	"time"
)

type Email_template struct{
	
	IdEmailTemplate int64
	NicknameOwner string
	EmailTemplateName string
	FilepathUrl string
	CreationDate time.Time
	Status bool 
	Erased bool 
	Sender string 
	Passverification string 
	Token string
	Subject string
	NicknameCreator string 
	SenderTag string 
		
}

 type LogEmail struct{	
	IdLogEmail int64  `gorm:"primary_key"`
	NicknameOwner string
	IdEmailTemplate int64
	ShippingDate string
	StateShippingCco bool
	StateShippingCc bool
 }

 type LogEmailReceiver struct{
	 IdLogEmail int64
	 IdContact int64
	 IdContactInformation int64
	 Receiver bool
	 Cc bool
	 Cco bool
	 Open bool
	 OpenDate *string
 }


type OutputIdEmail struct{
	IdEmail int `json:"id_email"` 
}

type ContactInformation struct{
	IdContactInformation int64 `gorm:"primary_key"`
	EmailTelephone string
	State bool
	TypeInfor string
	Erased bool
}

type QueryContactLinkInformation struct{
	IdContactInformation int64
	IdContact int64
}

type ResponseContactId struct{
	ContactID int `gorm:"contact_id"`
} 
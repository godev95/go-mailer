package route

import (
	"github.com/controller"
	"github.com/gin-gonic/gin"
)

func simplemail(c *gin.Context) {
	controller.Simplemail(c)
}

func emailBody(c *gin.Context) {

	controller.EmailBody(c)
}

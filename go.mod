module go-mailer

go 1.16

require (
	github.com/controller v0.0.0 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/helper v0.0.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/model v0.0.0 // indirect
	github.com/route v0.0.0
)

replace (
	github.com/controller => ./controller
	github.com/helper => ./helper
	github.com/model => ./model
	github.com/route => ./route
)
